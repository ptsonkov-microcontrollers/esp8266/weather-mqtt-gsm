#include <Adafruit_BME280.h>
#include <Adafruit_Sensor.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#include "code_secrets.h"

// Define modem:
#define TINY_GSM_MODEM_SIM800

// Set serial for AT commands (to the module)
SoftwareSerial ModemSerialAT(D5, D6); // RX, TX

#define TINY_GSM_DEBUG Serial

// Range to attempt to autobaud
#define GSM_AUTOBAUD_MIN 9600
#define GSM_AUTOBAUD_MAX 115200

// set GSM PIN, if any
#define GSM_PIN ""

// Your GPRS credentials, if any
const char apn[] = "telenorbg";
const char gprsUser[] = "";
const char gprsPass[] = "";

// MQTT details
#define mqtt_server SECRET_MQTT_SERVER
#define mqtt_port SECRET_MQTT_PORT
#define mqtt_user SECRET_MQTT_USER
#define mqtt_pass SECRET_MQTT_PASS

#include <TinyGsmClient.h>
TinyGsm gsm_modem(ModemSerialAT);
TinyGsmClient client(gsm_modem);
PubSubClient mqttClient(client);
Adafruit_BME280 bme;

// MQTT topics
#define sensor_name "villa"
#define sensor_location "out"
#define topic_temperature sensor_name "/" sensor_location "/temperature"
#define topic_humidity sensor_name "/" sensor_location "/humidity"
#define topic_pressure sensor_name "/" sensor_location "/pressure"
#define topic_batt_state sensor_name "/" sensor_location "/battery_state"
#define topic_batt_percent sensor_name "/" sensor_location "/battery_percent"
#define topic_batt_voltage sensor_name "/" sensor_location "/battery_voltage"

// Pins
#define modem_pwr 10
#define blue_led D4

// Timers
int deep_sleep_timer = 900;
int no_network_sleep_timer = 180;
unsigned long connect_timeout = 60;
unsigned long timeNow = 0;
unsigned long timeDelta = 0;

// Set sensor measurements
float temperature, humidity, pressure;

// Set battery measurements
uint8_t chargeState = -99;
int8_t percent = -99;
uint16_t milliVolts = -9999;

// Forward function declaration
bool setup_gprs();
void reconnect_mqtt();
void get_sensor_data();
void publish_topic(const char *topic, const char *payload);
void sleep_now(int seconds);

void setup() {

  pinMode(blue_led, OUTPUT);
  digitalWrite(blue_led, LOW);
  pinMode(modem_pwr, OUTPUT);
  digitalWrite(modem_pwr, HIGH);

  Serial.begin(115200);

  Serial.println("Starting ESP");

  Serial.print("Check BME280 sensor... ");
  if (!bme.begin(0x76)) {
    Serial.println("FAIL! Check wiring!");
    while (1)
      ;
  } else {
    Serial.println("OK");
  }

  // If GPRS is connected, connect to MQTT to send data
  bool gprsConnected = setup_gprs();
  if (gprsConnected) {

    mqttClient.setServer(mqtt_server, mqtt_port);

    if (!mqttClient.connected()) {
      reconnect_mqtt();
    }

    get_sensor_data();
    gsm_modem.getBattStats(chargeState, percent, milliVolts);

    mqttClient.loop();

    Serial.println("Temperature: " + String(temperature) + " ºC");
    Serial.println("Humidity: " + String(humidity) + " %");
    Serial.println("Pressure: " + String(pressure) + " hPa");
    Serial.println("Battery state: " + String(chargeState));
    Serial.println("Battery charge: " + String(percent) + " %");
    Serial.println("Battery voltage :" + String(milliVolts) + "mV");

    // Publish to MQTT
    publish_topic(topic_temperature, String(temperature).c_str());
    publish_topic(topic_humidity, String(humidity).c_str());
    publish_topic(topic_pressure, String(pressure).c_str());
    publish_topic(topic_batt_state, String(chargeState).c_str());
    publish_topic(topic_batt_percent, String(percent).c_str());
    publish_topic(topic_batt_voltage, String(milliVolts).c_str());
  } else {
    Serial.println("Network failure, sleep for " +
                   String(no_network_sleep_timer) + " seconds");
    sleep_now(no_network_sleep_timer);
  }

  Serial.println("MQTT disconnect");
  mqttClient.disconnect();

  delay(1000);
  Serial.print("Sleep for ");
  Serial.print(deep_sleep_timer);
  Serial.println(" seconds");
  sleep_now(deep_sleep_timer);
}

void loop() {}

void sleep_now(int seconds) { ESP.deepSleep(seconds * 1e6, WAKE_RF_DISABLED); }

bool setup_gprs() {
  Serial.println("Power ON modem");
  digitalWrite(modem_pwr, HIGH);

  Serial.println("Set modem baudrate");
  TinyGsmAutoBaud(ModemSerialAT, GSM_AUTOBAUD_MIN, GSM_AUTOBAUD_MAX);

  Serial.println("Initializing modem");
  gsm_modem.init();

  String modemInfo = gsm_modem.getModemInfo();
  Serial.print("Modem Info: ");
  Serial.println(modemInfo);

  if (GSM_PIN && gsm_modem.getSimStatus() != 3) {
    Serial.println("Unlock SIM with PIN");
    gsm_modem.simUnlock(GSM_PIN);
  }

  Serial.print("Waiting for network ");
  if (!gsm_modem.waitForNetwork()) {
    Serial.println(" fail");
    return false;
  }
  Serial.println(" success");

  Serial.print(F("Connecting to APN: "));
  Serial.print(apn);
  if (!gsm_modem.gprsConnect(apn, gprsUser, gprsPass)) {
    Serial.println(" fail");
    return false;
  }
  Serial.println(" success");

  if (gsm_modem.isGprsConnected()) {
    Serial.println("GPRS connected");
    return true;
  }
  return false;
}

void reconnect_mqtt() {
  timeNow = millis() / 1000;
  while (!mqttClient.connected()) {
    Serial.print("Attempting MQTT connection...");
    if (mqttClient.connect("ESP8266Client", mqtt_user, mqtt_pass)) {
      Serial.println("connected");
    } else {
      timeDelta = millis() / 1000;
      if (timeDelta - timeNow >= connect_timeout) {
        Serial.println("MQTT connection timeout, sleep for " +
                       String(deep_sleep_timer) + " seconds");
        sleep_now(deep_sleep_timer);
      }
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(", try again in 5 seconds");
      delay(5000);
    }
  }
}

void get_sensor_data() {
  temperature = bme.readTemperature();
  humidity = bme.readHumidity();
  pressure = bme.readPressure() / 100.0F;
}

void publish_topic(const char *topic, const char *payload) {
  Serial.println("Publish topic " + String(topic));
  mqttClient.publish(topic, payload, true);
}
